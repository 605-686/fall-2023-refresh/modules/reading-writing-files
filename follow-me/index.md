---
title: Reading/Writing Files
---

## Introduction

Many programs need to save or load data to/from files. Android file I/O is similar to Java I/O (using the same APIs), but depending on where you want to save/load files, you may need to jump through a few hoops in the name of user privacy and protection.

In this module, we'll look at simple file I/O in private directories on a device or SD card, and more complex Storage-Access Framework file I/O.

> Note: Due to out-of-town trips, I decided to edit a recording of a live lecture that I gave Summer 2021.

## Videos

Total video time for this module: 1:59:57

            
### Files: Lecture (Summer 2021) (27:29)

<iframe width="800" height="450" src="https://www.youtube.com/embed/8mR8lqTETtE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                

### Files: Example 1 - Private Files (Summer 2021) (35:04)

<iframe width="800" height="450" src="https://www.youtube.com/embed/4g3ScXm31tg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Files: Example 2 - SD Card Private Files and MediaStore (Summer 2021) (35:52)

<iframe width="800" height="450" src="https://www.youtube.com/embed/EAJPl4xdAxY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Files: Example 3 - Storage Access Framework (Summer 2021) (21:32)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Fr_XW94jPIQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/605-686/fall-2023-refresh/modules/reading-writing-files](https://gitlab.com/605-686/fall-2023-refresh/modules/reading-writing-files)